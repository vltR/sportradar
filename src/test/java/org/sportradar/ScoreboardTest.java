package org.sportradar;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.sportradar.model.Game;
import org.sportradar.model.Scoreboard;
import org.sportradar.model.Team;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

public class ScoreboardTest {

    private static Scoreboard scoreboard;

    private static Team mexico;
    private static Team uruguay;

    @BeforeClass
    public static void init() {
        scoreboard = new Scoreboard();

        mexico = TeamDb.forName("Mexico");
        uruguay = TeamDb.forName("Uruguay");
    }

    @Test
    public void finishGameTest() {
        scoreboard.addGame("Mexico", "Uruguay");
        scoreboard.finish("Mexico", "Uruguay");
        assertTrue(scoreboard.findGame("Mexico", "Uruguay").get().isFinished());
    }

    @Test
    public void findGameTest() {
        scoreboard.addGame("Mexico", "Uruguay");
        Optional<Game> game = scoreboard.findGame("Mexico", "Uruguay");
        assertTrue(game.isPresent());
    }

    @Test
    public void testOrder() {
        scoreboard.addGame("Mexico", "Canada");
        scoreboard.addGame("Spain", "Brazil");
        scoreboard.addGame("Germany", "France");
        scoreboard.addGame("Argentina", "Australia");
        scoreboard.addGame("Uruguay", "Italy");

        scoreboard.setScore("Uruguay", "Italy", 6, 6);
        scoreboard.setScore("Spain", "Brazil", 10, 2);
        scoreboard.setScore("Mexico", "Canada", 0, 5);
        scoreboard.setScore("Argentina", "Australia", 3, 1);
        scoreboard.setScore("Germany", "France", 2, 2);

        List<Game> sorted = scoreboard.forPrint();
        for (Game game : sorted) {
            System.out.println(game);
        }
        assertTrue(sorted.get(0).getHomeTeam().getName().equalsIgnoreCase("Uruguay"));
        assertTrue(sorted.get(1).getHomeTeam().getName().equalsIgnoreCase("Spain"));
        assertTrue(sorted.get(2).getHomeTeam().getName().equalsIgnoreCase("Mexico"));
        assertTrue(sorted.get(3).getHomeTeam().getName().equalsIgnoreCase("Argentina"));
        assertTrue(sorted.get(4).getHomeTeam().getName().equalsIgnoreCase("Germany"));
    }
}
