package org.sportradar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.sportradar.model.Team;

public class TeamDbTest {

    @Test
    public void addTest() {
        Team team = new Team("Team");
        TeamDb.add(team);
        assertTrue(team.getId() != null);
    }

    @Test
    public void getBySymbolTest() {
        Team team = new Team("Team");
        TeamDb.add(team);
        assertTrue(TeamDb.forName("ASD") != null);
    }
}
