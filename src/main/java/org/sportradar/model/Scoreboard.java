package org.sportradar.model;

import org.sportradar.TeamDb;

import java.util.*;
import java.util.stream.Collectors;

public class Scoreboard {

    private Set<Game> gamesSet;

    public Scoreboard() {
        gamesSet = Collections.synchronizedSet(new HashSet<>());
    }

    public Game addGame(String home, String away) {
        if (findGame(home, away).isEmpty()) {
            Team homeTeam = TeamDb.forName(home);
            Team awayTeam = TeamDb.forName(away);
            if (homeTeam != null && awayTeam != null) {
                Game game = new Game(homeTeam, awayTeam);
                gamesSet.add(game);
                game.setId((long) gamesSet.size());
                return game;
            }
        }
        return null;
    }


    public void setScore(String home, String away, Integer homeScore, Integer awayScore) {
        Optional<Game> game = findGame(home, away);
        if (game.isPresent()) {
            if (homeScore != null) {
                game.get().setHomeScore(homeScore);
            }
            if (awayScore != null) {
                game.get().setAwayScore(awayScore);
            }
        }
    }

    public Optional<Game> findGame(String home, String away) {
        if (home != null && away != null) {
            Team homeTeam = TeamDb.forName(home);
            Team awayTeam = TeamDb.forName(away);
            if (homeTeam == null || awayTeam == null) {
                return Optional.empty();
            }
            Optional<Game> game = gamesSet.stream()
                    .filter(g -> {
                        return g.getHomeTeam().getId().equals(homeTeam.getId())
                                && g.getAwayTeam().getId().equals(awayTeam.getId());
                    }).findFirst();
            return game;
        }
        return Optional.empty();
    }

    public void finish(String home, String away) {
        Optional<Game> game = findGame(home, away);
        if (game.isPresent()) {
            game.get().finish();
        }
    }

    public List<Game> forPrint() {
        Comparator<Game> comparator = (g1, g2) -> {
            int ret = g2.getTotalScore() - g1.getTotalScore();
            if (ret == 0) {
                return (int) (g2.getId() - g1.getId());
            }
            return ret;
        };

        return gamesSet.stream().filter(g -> !g.isFinished()).sorted(comparator).collect(Collectors.toList());
    }

}
