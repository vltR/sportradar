package org.sportradar.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.TemporalField;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class Game {

    private Long id;
    private AtomicReference<GameStatus> status;
    private Team homeTeam;
    private Team awayTeam;
    private AtomicInteger homeScore;
    private AtomicInteger awayScore;

    public Game(Team home, Team away) {
        setHomeTeam(home);
        setAwayTeam(away);
        homeScore = new AtomicInteger(0);
        awayScore = new AtomicInteger(0);
        status = new AtomicReference<>(GameStatus.LIVE);
    }

    public int getTotalScore() {
        return homeScore.get() + awayScore.get();
    }

    public String toString() {
        StringJoiner sj = new StringJoiner(" ");
        sj.add(homeTeam.getName());
        sj.add("" + homeScore.get());
        sj.add("-");
        sj.add(awayTeam.getName());
        sj.add("" + awayScore.get());
        return sj.toString();
    }

    public void finish() {
        status.set(GameStatus.FINISHED);
    }

    public boolean isFinished() {
        return status.get().equals(GameStatus.FINISHED);
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public void setHomeScore(Integer score) {
        homeScore.set(score);
    }

    public void setAwayScore(Integer score) {
        awayScore.set(score);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
