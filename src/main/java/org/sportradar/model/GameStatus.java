package org.sportradar.model;

public enum GameStatus {
    LIVE,
    FINISHED;
}
