package org.sportradar;

import org.sportradar.model.Game;
import org.sportradar.model.Team;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class TeamDb {

    private static Set<Team> teams = new HashSet<>();

    static {
        add(new Team("Mexico"));
        add(new Team("Uruguay"));
        add(new Team("Canada"));
        add(new Team("Spain"));
        add(new Team("Argentina"));
        add(new Team("Australia"));
        add(new Team("Brazil"));
        add(new Team("Germany"));
        add(new Team("Italy"));
        add(new Team("France"));

    }

    public static Team forName(String name) {
        Optional<Team> found = teams.stream().filter(t -> t.getName().equalsIgnoreCase(name)).findFirst();
        if (found.isPresent()) {
            return found.get();
        }
        return null;
    }

    public static void add(Team team) {
        teams.add(team);
        team.setId((long) teams.size());
    }

}
